# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=dracut
pkgver=056
pkgrel=1
pkgdesc="An event driven initramfs infrastructure"
arch=('x86_64')
url="https://dracut.wiki.kernel.org"
license=('GPL')
groups=('base')
depends=('pkg-config' 'systemd' 'util-linux' 'cpio')
makedepends=('bash-completion')
backup=(etc/dracut.conf
    etc/dracut.conf.d/myflags.conf)
source=(https://mirrors.edge.kernel.org/pub/linux/utils/boot/${pkgname}/${pkgname}-${pkgver}.tar.xz
    myflags.conf
    dracut-install
    dracut-remove
    90-dracut-install.hook
    60-dracut-remove.hook)
sha256sums=(e025bbdce9d1209640fb3f5f674f059c7e1f441537ba421703fe56055502421d
    c74e407138f37c9636aa4da2056b400939d579177192b80a3d99c1e62c312813
    e1ba41ab5690527759b7370b4af2dc836796e4c31f2d5f4dbedae6675f9664fd
    8d7fe6622dcbe5fb8a4b0df33265e82bd895e328d202a841a46859c1dd99d47e
    de09e8e65837b189aec0a8c9a067143880faff14467a5573949f772f39c053b3
    e79f8e9572c5d1af6052104eac7ff956754f7a191b52b16adf12b65a38e9b4ed)

build() {
    cd ${pkgname}-${pkgver}

    ./configure             \
        --prefix=/usr       \
        --bindir=/usr/sbin  \
        --sysconfdir=/etc   \
        --libdir=/usr/lib   \
        --systemdsystemunitdir=/usr/lib/systemd/system \
        --bashcompletiondir=$(pkg-config --variable=completionsdir bash-completion)

    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install

    install -vm644 ${srcdir}/myflags.conf ${pkgdir}/etc/dracut.conf.d/

    install -vDm644 ${srcdir}/90-dracut-install.hook ${pkgdir}/usr/share/libalpm/hooks/90-dracut-install.hook
    install -vDm644 ${srcdir}/60-dracut-remove.hook  ${pkgdir}/usr/share/libalpm/hooks/60-dracut-remove.hook
    install -vDm755 ${srcdir}/dracut-install         ${pkgdir}/usr/share/libalpm/scripts/dracut-install
    install -vDm755 ${srcdir}/dracut-remove          ${pkgdir}/usr/share/libalpm/scripts/dracut-remove
}
